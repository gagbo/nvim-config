local nvim_plugins_path = vim.fn.stdpath("data") .. "/lazy"

-- Bootstrap lazy.nvim
local lazy_path = nvim_plugins_path .. "/lazy.nvim"
if not vim.loop.fs_stat(lazy_path) then
  vim.notify("Bootstrapping lazy.nvim...", vim.log.levels.INFO)
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "--single-branch",
    "https://github.com/folke/lazy.nvim.git",
    lazy_path,
  })
end

-- Bootstrap hotpot.nvim
local hotpot_path = nvim_plugins_path .. "/hotpot.nvim"
if not vim.loop.fs_stat(hotpot_path) then
  vim.notify("Bootstrapping hotpot.nvim...", vim.log.levels.INFO)
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "--single-branch",
    "https://github.com/rktjmp/hotpot.nvim.git",
    hotpot_path,
  })
end

-- Add lazy.nvim to rtp
vim.opt.runtimepath:prepend(lazy_path)

-- Add hotpot.nvim to rtp
vim.opt.runtimepath:prepend(hotpot_path)

-- Generate plugins table
local plugins = {
  {
    "rktjmp/hotpot.nvim",
  },
}

-- Enable vim loader as per checkhealth recommendation
vim.loader.enable()

-- Configure hotpot.nvim
require("hotpot").setup({
  provide_require_fennel = true,
})

require("config")

-- Add plugins to table
local config_plugins_path = vim.fn.stdpath("config") .. "/fnl/config/plugins"
if vim.loop.fs_stat(config_plugins_path) then
  for file in vim.fs.dir(config_plugins_path) do
    file = file:match("^(.*)%.fnl$")
    if vim.g.neovide and file == "noice" then
    --[[ NOTE
      Lua doesn't have "continue" so have to use
      empty consequent blocks instead, and
      make sure that this if statement is the only one that matters in
      the for-loop body
      Tracking issue: https://github.com/folke/noice.nvim/issues/17
    ]]
    else
        plugins[#plugins + 1] = require("config.plugins." .. file)
    end
  end
end

-- Configure lazy.nvim
require("lazy").setup(
  plugins,
  { install = { missing = true } })

-- Call post plugins code
-- require("post-plugins")
