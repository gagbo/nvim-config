#!/usr/bin/env python3
# Author : Bjorn @neersighted Neergaard <bjorn@neersighted.com>
"""Profile (n)vim startup time."""

from collections import OrderedDict
from decimal import Decimal
from itertools import chain
from os.path import basename
from re import compile as re
from shutil import get_terminal_size
from subprocess import run
from sys import argv
from tempfile import NamedTemporaryFile
from typing import IO, Iterable, MutableMapping, Text

SEP_F = " {} "
KEY_F = "{}: {:06.3f}ms "
CHART_F = "\n{s}\n{b}"

LINE_RE = re(r"^(?:\s*(\d{3}\.\d{3})){2,3}: (.*)$")
VIML_RE = re(r"^sourcing ((?!vimrc file\(s\)).*)$")
PLUG_RE = re(r"^(?:/[^\0/]+)+/pack/[^\0/]+/start/([^\0/]+)/((?:/?[^\0/]+)+)$")
RUNT_RE = re(r"/share/n?vim/|\$N?VIM")
VIMRC_RE = re(r"/\.vim/|/\.config/nvim/|/\.local/share/nvim/|/\.?g?vimrc")

COLS, _ = get_terminal_size()


class Subprofile(OrderedDict, MutableMapping[str, Decimal]):
    """A subcategory in a Vim startup profile."""

    def proportions(self) -> Iterable[float]:
        """Returns proportional values (percentage of largest)."""
        largest = max(self.values())
        yield from (float(val / largest) for val in self.values())

    def __missing__(self, key: str) -> int:
        return 0


# pylint: disable=too-few-public-methods
class Profile():
    """A complete Vim startup profile."""
    vim = Subprofile()
    runtime = Subprofile()
    vimrc = Subprofile()
    plugins = Subprofile()
    other = Subprofile()

    @property
    def times(self) -> Iterable[Decimal]:
        """Iterate all times."""
        return chain(self.vim.values(), self.viml_times)

    @property
    def viml_times(self) -> Iterable[Decimal]:
        """Iterate all times spent in VimL scripts."""
        return chain(
            self.runtime.values(),
            self.vimrc.values(),
            self.plugins.values(),
            self.other.values(),
        )


def gen_seper(title: Text, fill: Text = "=") -> Text:
    """Generate a section seperator."""
    return SEP_F.format(title).center(COLS, fill)


def gen_chart(title: Text, profile: Subprofile) -> Text:
    """Generate a chart of members of a Subprofile."""
    if not profile:
        return ""

    key_width = len(max(profile.keys(), key=len)) + len(KEY_F.format("", 0))
    bar_width = COLS - key_width

    seperator = gen_seper(f"{title} ({sum(profile.values())}ms)", "-")

    keys = (
        KEY_F.format(key, value).rjust(key_width)
        for key, value in profile.items()
    )

    bars = (
        key + "*" * int(proportion * bar_width)
        for key, proportion in zip(keys, profile.proportions())
    )

    return CHART_F.format(s=seperator, b="\n".join(bars))


def parse_startup(log: IO[str]) -> Profile:
    """Parse a Vim startup log into a Profile."""
    profile = Profile()

    for line_m in filter(None, (LINE_RE.match(line) for line in log)):
        subprofile = profile.other
        value = Decimal(line_m[1])
        key = line_m[2]

        viml_m = VIML_RE.match(key)
        if viml_m:
            key = viml_m[1]

            plug_m = PLUG_RE.match(key)
            if plug_m:
                key = plug_m[1]
                subprofile = profile.plugins
            elif RUNT_RE.search(key):
                subprofile = profile.runtime
            elif VIMRC_RE.search(key):
                subprofile = profile.vimrc
        else:
            subprofile = profile.vim

        subprofile[key] += value
    return profile


def main() -> None:
    """Profile (n)vim startup time."""
    vim = "vim"
    if basename(argv[0]) == "nvimprofile":
        vim = "nvim"

    with NamedTemporaryFile(mode="r") as startup_log:
        run(
            [
                vim,
                "--startuptime",
                startup_log.name,
                *argv[1:],
                "-c",
                "noautocmd qall",
            ]
        )
        profile = parse_startup(startup_log)

    times = f"{sum(profile.times)}ms ({sum(profile.viml_times)}ms in scripts)"

    print(gen_seper(f"{vim} Startup Profile"))
    print(gen_chart(vim.upper(), profile.vim))
    print(gen_chart("RUNTIME", profile.runtime))
    print(gen_chart("VIMRC", profile.vimrc))
    print(gen_chart("PLUGINS", profile.plugins))
    print(gen_chart("OTHER", profile.other))
    print(gen_seper(f"{vim} ready in {times}"))


if __name__ == "__main__":
    main()
