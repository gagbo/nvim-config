# Fennel based neovim init

This is a test to get all the lisp goodness in a neovim configuration. Also
mostly doing some lisp to cool off.

**Note** This toy probably needs Neovim 0.7.0+, because I didn't care about
extra compat

## State

- [X] As usable as Vanilla neovim
- [ ] Can run Fennel commands to affect running Neovim
- [ ] Get complex enough to declare +emacs+ neovim bankruptcy

## Features

### Neovim Lua "bootstrap"

A few hacks in the
initialization are used to defer the init to Lua, and then Fennel.

### Fennel as first party language

Everything happens in the `init` function of [config.fnl](./fnl/config/init.fnl),
and then in all the surrounding modules, including bootstrapping packer.nvim,
and all plugin configuration (including GUIs; they're plugins right?)

### Packer.nvim for plugin management

The plugin list is declared simply in a table variable in
[plugins.fnl](./fnl/config/plugins.fnl) file, and Packer handles all the rest by
providing `:Packer*` commands in Neovim later.

## Tentative roadmap

### Devtools in the config

I'll edit this in emacs for the time being, but having the Neovim API doc
somewhere handy and completion in Fennel would be awesome

### Automatically reload and PackerSync on plugins.fnl changes

For the QoL.

### Add a safe-packadd and safe-require to utils

Just so the config never crashes on plugins unsync, but instead shows a message
with more info like 

> FOO is missing, maybe you need to :PackerSync

### Automatically packadd nvim lua packages

The packadd is necessary to update the runtimepath, which triggers an update of
lua package.path and package.cpath

Without this, the `require "XXX"` calls fail

So it would be nice to handle this transparently from the config

### Find an actual good solution to replace magit

And not instantly cry whenever I hit `SPC g g`.

### Fix load order of direnv.vim vs LSP server start

For the time being, when I go in a JS project that only has the LSP servers
through direnv (via `layout node` or something, I need to:
- hit `RET` a bunch of times to pass error messages
- do `:e` to reload the buffer
- hit `RET` a bunch of times to pass "expected" error messages (like tailwindcss
  server missing if I'm in a project that doesn't use tailwind)
