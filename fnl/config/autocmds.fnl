(lambda init []
  "Set all Autocommands"
  (let [utils (require :config.utils)
        augroup utils.augroup]

    ;;; Insert mode in terminals
    (vim.api.nvim_create_autocmd
         [:TermOpen]
         {:group (augroup "InsertInTerm")
              :command "startinsert"})

    ;;; Highlight on yank
    (vim.api.nvim_create_autocmd
         [:TextYankPost]
         {:group (augroup "HlOnYank")
              :callback (lambda [] (vim.highlight.on_yank
                                        {:higroup :IncSearch
                                             :timeout 300
                                             :on_visual false}))})

    ;;; Check if we need to reload file
    (vim.api.nvim_create_autocmd
         [:FocusGained :TermClose :TermLeave]
         {:group (augroup "checktime")
              :command "checktime"})

    ;;; Resize splits when window gets resized
    (vim.api.nvim_create_autocmd
         [:VimResized]
         {:group (augroup "ResizeSplits")
              :callback (lambda [] (vim.cmd "tabdo wincmd ="))})

    ;;; Go to last location when opening a buffer
    (vim.api.nvim_create_autocmd
         [:BufReadPost]
         {:group (augroup "LastLoc")
              :callback (lambda []
                          (let [mark (vim.api.nvim_buf_get_mark 0 "\"")
                                lcount (vim.api.nvim_buf_line_count 0)
                                lmark (. mark 1)]
                              (when (and (> lmark 0) (<= lmark lcount))
                                (pcall vim.api.nvim_win_set_cursor 0 mark))))})

    ;;; Close some popup-like filetypes with q
    (vim.api.nvim_create_autocmd
         [:FileType]
         {:group (augroup "CloseWithQ")
              :pattern [:PlenaryTestPopup
                            :help
                            :lspinfo
                            :man
                            :notify
                            :qf
                            :spectre_panel
                            :startuptime
                            :tsplayground
                            :checkhealth
                            :neotest-output
                            :neotest-summary
                            :neotest-output-panel]
              :callback (lambda [event]
                              (tset (. vim.bo event.buf) :buflisted false)
                              (utils.fn-buffer-mapper "n" "q" "<cmd>close<cr>" event.buf))})

    ;;; Auto create missing directories on save
    (vim.api.nvim_create_autocmd
         [:BufWritePre]
         {:group (augroup "AutoCreateDir")
              :callback (lambda [event]
                              (if (event.match:match "^%w%w+://")
                                 nil
                                 (let [file (or (vim.loop.fs_realpath event.match) event.match)]
                                    (vim.fn.mkdir (vim.fn.fnamemodify file ":p:h") :p))))})))

{: init}
