(lambda init []
  "Set all options"
  (set vim.o.hlsearch true)
  (set vim.o.inccommand "split")
  (set vim.o.confirm true)

  (set vim.o.grepformat "%f:%l:%c:%m")
  (set vim.o.grepprg "rg --vimgrep")

  (set vim.o.pumblend 10)
  (set vim.o.pumheight 10)

  (set vim.o.scrolloff 4)
  (set vim.o.sidescrolloff 4)

  (set vim.o.undofile true)
  (set vim.o.undolevels 10000)

  (set vim.o.winminwidth 5)

  (when (= (vim.fn.has :nvim-0.9.0) 1)
      (set vim.o.splitkeep :screen))

  ;; Clipboard sharing
  (vim.cmd "set clipboard+=unnamedplus")

  (set vim.o.mouse "a")
  (set vim.o.mousemodel "popup")

  (set vim.o.breakindent true)

  (set vim.o.number true)
  (set vim.o.relativenumber true)

  (set vim.o.ignorecase true)
  (set vim.o.smartcase true)
  (set vim.o.infercase false)

  (set vim.o.shortmess "a")

  (set vim.o.cursorline true)

  (set vim.o.diffopt "filler,vertical")

  (set vim.o.hidden true)
  (set vim.o.switchbuf "useopen,usetab,split")
  (set vim.o.listchars "tab:»·,trail:·,nbsp:~")
  (set vim.o.list true)

  (set vim.o.wildmode "longest:full,full")

  (set vim.bo.shiftwidth 4)
  (set vim.bo.softtabstop (- 1))
  (set vim.bo.expandtab true)

  (set vim.o.complete ".,w,b,u")

  (set vim.o.updatetime 300)
  (set vim.o.completeopt "menuone,menu,preview,noinsert")

  (set vim.o.splitbelow true)
  (set vim.o.splitright true))

{: init}
