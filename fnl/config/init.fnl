(local utils (require :config.utils))

(let [bindings (require :config.bindings)]
  (bindings.leader (utils.kbd "<Space>") (utils.kbd ","))
  (bindings.bare))

;; Options
(let [options (require :config.options)]
  (options.init))

;; UI
(vim.cmd "syntax enable")
(vim.cmd "filetype plugin indent on")
(set vim.o.termguicolors true)

(let [gui (require :config.gui)]
  (gui.init))

;; Autocommands
(let [autocmds (require :config.autocmds)]
  (autocmds.init))
