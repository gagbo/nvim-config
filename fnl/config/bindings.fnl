;;; bindings.fnl --- Mappings

(local utils (require :config.utils))

(local mapper utils.mapper)
(local noremapper utils.noremapper)
(local fn-mapper utils.fn-mapper)

(lambda diagnostic-goto [next ?severity]
  "Build a function that goes to the next diagnostic matching the given direction and the severity"
  (let [go (if next vim.diagnostic.goto_next
                vim.diagnostic.goto_prev)
        severity (or (and ?severity (. vim.diagnostic.severity ?severity)) nil)]
    (fn [] (go {: severity}))))

(lambda bare []
  "Bare bindings."

  ;; Better diagnostics navigation
  (fn-mapper "n" "[d" (diagnostic-goto false))
  (fn-mapper "n" "]d" (diagnostic-goto true))
  (fn-mapper "n" "[e" (diagnostic-goto false "ERROR"))
  (fn-mapper "n" "]e" (diagnostic-goto true "ERROR"))
  (fn-mapper "n" "[w" (diagnostic-goto false "WARN"))
  (fn-mapper "n" "]w" (diagnostic-goto true "WARN"))

  ;; Undo as U
  (mapper "" "U" "<C-r>")

  ;; Tab to toggle folds (:h zA)
  (mapper "n" "<Tab>" "zA")

  ;; Esc to trigger Nohl
  (mapper "n" "<esc>" "<esc><cmd>nohl<cr>")

;;; Visual mode
  (mapper "v" "<" "<gv")
  (mapper "v" ">" ">gv")

  (mapper "v" "<A-j>" "<cmd>m .+1<CR>==")
  (mapper "v" "<A-k>" "<cmd>m .-2<CR>==")
  (mapper "v" "p" "\"_dP")

  (mapper "x" "J" "<cmd>m '>+1<CR>gv-gv")
  (mapper "x" "K" "<cmd>m '<-2<CR>gv-gv")
  (mapper "x" "<A-j>" "<cmd>m '>+1<CR>gv-gv")
  (mapper "x" "<A-k>" "<cmd>m '<-2<CR>gv-gv")
;;; Terminal
  (mapper "t" "<esc>" "<C-\\><C-N>")
  (mapper "t" "<C-p>" "<C-\\><C-N><C-w>h")
  (mapper "t" "<C-t>" "<C-\\><C-N><C-w>j")
  (mapper "t" "<C-s>" "<C-\\><C-N><C-w>k")
  (mapper "t" "<C-r>" "<C-\\><C-N><C-w>l"))

(lambda leader [leader-key localleader-key]
  "General leader bindings"
  (let [
        leader
        (lambda [mode key result]
          (mapper mode (.. leader-key key) result))
        fn-leader
        (lambda [mode key result]
          (fn-mapper mode (.. leader-key key) result))
        ]

    (set vim.g.mapleader leader-key)
    (set vim.g.maplocalleader localleader-key)

    ;; B : Buffers
    (leader "n" "bd" "<cmd>bdelete<CR>")
    (leader "n" "bD" "<cmd>bdelete!<CR>")
    (leader "n" "bn" "<cmd>bnext<CR>")
    (leader "n" "bp" "<cmd>bprevious<CR>")
    ;; C : Code
    (fn-leader "n" "cx" vim.diagnostic.open_float)
    ;; W : Window
    (leader "n" "ww" "<C-w>w")
    (leader "n" "wh" "<C-w>h")
    (leader "n" "wj" "<C-w>j")
    (leader "n" "wk" "<C-w>k")
    (leader "n" "wl" "<C-w>l")
    (leader "n" "wv" "<C-w>v")
    (leader "n" "ws" "<C-w>s")
    (leader "n" "wq" "<C-w>q")
    (leader "n" "wo" "<C-w>o")
    ;; F : File
    (leader "n" "fs" "<cmd>w<CR>")
    (leader "n" "fS" ":sav")
    ;; O : Open
    (leader "n" "ot" "<cmd>split +terminal<CR>")
    ;; P : Projects
    (leader "n" "pp" "<cmd>Telescope projects<CR>")
    ;; Q : Quit
    (leader "n" "qQ" "<cmd>qa<CR>")
    ))

(lambda bepo []
  "Remap keys for bépo layout."

  ;; {W} -> [É]
  ;; ——————————
  ;; On remappe W sur É :
  (noremapper "" "é" "w" )
  (noremapper "" "É" "W" )
  ;; Corollaire: on remplace les text objects aw, aW, iw et iW
  ;; pour effacer/remplacer un mot quand on n’est pas au début (daé / laé).
  (mapper "o" "aé" "aw")
  (mapper "o" "aÉ" "aW")
  (mapper "o" "ié" "iw")
  (mapper "o" "iÉ" "iW")
  ;; Pour faciliter les manipulations de fenêtres, on utilise {W} comme un Ctrl+W :
  (noremapper "" "w" "<C-w>")
  (noremapper "" "W" "<C-w><C-w>")

  ;; [HJKL] -> {CTSR}
  ;; ————————————————
  ;; {cr} = « gauche / droite »
  (noremapper "" "c" "h" )
  (noremapper "" "r" "l" )
  ;; {ts} = « haut / bas »
  (noremapper "" "t" "j" )
  (noremapper "" "s" "k" )
  ;; {CR} = « haut / bas de l'écran »
  (noremapper "" "C" "H" )
  (noremapper "" "R" "L" )
  ;; {TS} = « joindre / aide »
  (noremapper "" "T" "J" )
  (noremapper "" "S" "K" )
  ;; Corollaire : repli suivant / précédent
  (mapper "" "zs" "zj")
  (mapper "" "zt" "zk")

  ;; {HJKL} <- [CTSR]
  ;; ————————————————
  ;; {J} = « Jusqu'à »            (j = suivant, J = précédant)
  (noremapper "" "j" "t" )
  (noremapper "" "J" "T" )
  ;; {L} = « Change »             (l = attend un mvt, L = jusqu'à la fin de ligne)
  (noremapper "" "l" "c" )
  (noremapper "" "L" "C" )
  ;; {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
  (noremapper "" "h" "r" )
  (noremapper "" "H" "R" )
  ;; {K} = « Substitue »          (k = caractère, K = ligne)
  (noremapper "" "k" "s" )
  (noremapper "" "K" "S" )
  ;; Corollaire : correction orthographique
  (mapper "" "]k" "]s")
  (mapper "" "[k" "[s")

  ;; Désambiguation de {g}
  ;; —————————————————————
  ;; ligne écran précédente / suivante (à l'intérieur d'une phrase)
  (mapper "" "gs" "gk")
  (mapper "" "gt" "gj")
  ;; onglet précédent / suivant
  (mapper "" "gb" "gT")
  (mapper "" "gé" "gt")
  ;; optionnel : {gB} / {gÉ} pour aller au premier / dernier onglet
  (mapper "" "gB" ":exe \"silent! tabfirst\"<CR>")
  (mapper "" "gÉ" ":exe \"silent! tablast\"<CR>")
  ;; optionnel : {g"} pour aller au début de la ligne écran
  (mapper "" "g\"" "g0")

  ;; <> en direct
  ;; ————————————
  (noremapper "" "«" "<" )
  (noremapper "" "»" ">" )

  ;; Remaper la gestion des fenêtres
  ;; ———————————————————————————————
  (mapper "" "wt" "<C-w>j")
  (mapper "" "ws" "<C-w>k")
  (mapper "" "wc" "<C-w>h")
  (mapper "" "wr" "<C-w>l")
  (mapper "" "wd" "<C-w>c")
  (mapper "" "wo" "<C-w>s")
  (mapper "" "wp" "<C-w>o")
  (mapper "" "w<SPACE>" ":split<CR>")
  (mapper "" "w<CR>" ":vsplit<CR>")

  ;; è pour aller au début de ligne
  (noremapper "" "è" "^" )
  (noremapper "" "È" "0" ))

(lambda optimot []
  "Remap keys for optimot layout."

  ;; [HJKL] -> {PTSR}
  ;; ————————————————
  ;; {pr} = « gauche / droite »
  (noremapper "" "p" "h")
  (noremapper "" "r" "l")
  ;; {ts} = « haut / bas »
  (noremapper "" "t" "j" )
  (noremapper "" "s" "k" )
  ;; {PR} = « haut / bas de l'écran »
  (noremapper "" "P" "H" )
  (noremapper "" "R" "L" )
  ;; {TS} = « joindre / aide »
  (noremapper "" "T" "J" )
  (noremapper "" "S" "K" )
  ;; Corollaire : repli suivant / précédent
  (mapper "" "zs" "zj")
  (mapper "" "zt" "zk")

  ;; {HJKL} <- [PTSR]
  ;; ————————————————
  ;; {J} = « Jusqu'à »            (j = suivant, J = précédant)
  (noremapper "" "j" "t" )
  (noremapper "" "J" "T" )
  ;; {L} = « Coller »             (l = après/en-dessous, L = avant/au-dessus)
  (noremapper "" "l" "p" )
  (noremapper "" "L" "P" )
  ;; {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
  (noremapper "" "h" "r" )
  (noremapper "" "H" "R" )
  ;; {K} = « Substitue »          (k = caractère, K = ligne)
  (noremapper "" "k" "s" )
  (noremapper "" "K" "S" )
  ;; Corollaire : correction orthographique
  (mapper "" "]k" "]s")
  (mapper "" "[k" "[s")

  ;; Désambiguation de {g}
  ;; —————————————————————
  ;; ligne écran précédente / suivante (à l'intérieur d'une phrase)
  (mapper "" "gs" "gk")
  (mapper "" "gt" "gj")
  ;; optionnel : {èT} / {éT} pour aller au premier / dernier onglet
  (mapper "" "èT" ":exe \"silent! tabfirst\"<CR>")
  (mapper "" "éT" ":exe \"silent! tablast\"<CR>")
  ;; optionnel : {g"} pour aller au début de la ligne écran
  (mapper "" "g\"" "g0")

  ;; Unimpaired
  ;; ----------
  (mapper "" "é" "]")
  (mapper "" "è" "[")

  ;; <> en direct
  ;; ————————————
  (noremapper "" "«" "<" )
  (noremapper "" "»" ">" )

  ;; à pour aller au début de ligne
  (noremapper "" "à" "^" )
  (noremapper "" "À" "0" ))

;; TODO colorise les nbsp
;; highlight NbSp ctermbg=lightgray guibg=lightred
;; match NbSp /\%xa0/
;; endif

(lambda colemak-naive []
  "Remap keys for colemak mod dh layout."
  (mapper "" "m" "h")
  (mapper "" "M" "H")
  (mapper "" "n" "j")
  (mapper "" "N" "J")
  (mapper "" "e" "k")
  (mapper "" "E" "K")
  (mapper "" "i" "l")
  (mapper "" "I" "L")

  (mapper "" "h" "m")
  (mapper "" "H" "M")

  (mapper "" "u" "i")
  (mapper "o" "u" "i")
  (mapper "" "U" "I")

  (mapper "v" "l" "u")
  (mapper "" "l" "u")
  (mapper "v" "L" "U")

  (mapper "" "f" "e")
  (mapper "" "F" "E")
  (mapper "" "t" "f")
  (mapper "" "T" "F")
  (mapper "" "j" "t")
  (mapper "" "J" "T")

  (mapper "" "k" "n")
  (mapper "" "K" "N"))

(lambda colemak-langremap []
  "Remap keys for colemak mod dh layout."
  ;; Source github.com/wbolster/emacs-evil-colemak-basics (hnei.vim)
  ;;
  (set vim.o.langmap "mM;hH,nN;jJ,eE;kK,iI;lL,hH;mM,kK;nN,uU;iI,lL;uU,fF;eE,tT;fF,jJ;tT")
  (set vim.o.langremap false))

(lambda colemak-minimal []
  "Remap keys for colemak mod dh layout."

  (noremapper "" "n" "v:count == 0 ? 'gj' : 'j'" { :expr true })
  (noremapper "" "N" "J")
  (noremapper "" "e" "v:count == 0 ? 'gk' : 'k'" { :expr true })
  (noremapper "" "E" "K")

  (noremapper "" "s" "h")
  (noremapper "" "t" "l")

  (noremapper "" "f" "e")
  (noremapper "" "F" "E")
  (noremapper "" "k" "n")
  (noremapper "" "K" "N"))

{: bare
 : leader
 : colemak-naive
 : colemak-langremap
 : colemak-minimal
 : bepo
 : optimot}
