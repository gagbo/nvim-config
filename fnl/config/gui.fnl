;;; config/gui.fnl --- GUI specific settings


(lambda fvim []
  "Initialize the setup for FVim front-end."
  (when (. vim.g :fvim_loaded)
    (let [utils (require :config.utils)]
      (utils.mapper "n" "<C-ScrollWheelUp>" ":set guifont=+<CR>" "Zoom In")
      (utils.mapper "n" "<C-ScrollWheelDown>" ":set guifont=-<CR>" "Zoom Out")
      (utils.mapper "n" "<A-CR>" ":FVimToggleFullScreen<CR>" "Toggle FullScreen")
      (vim.cmd "FVimCursorSmoothMove v:true")
      (vim.cmd "FVimCursorSmoothBlink v:true")
      (vim.cmd "FVimBackgroundComposition 'acrylic'")
      (vim.cmd "FVimBackgroundOpacity 0.85")
      (vim.cmd "FVimBackgroundAltOpacity 0.85"))))

(lambda neovide []
  "Initialize the setup for Neovide."
  (when (. vim.g :neovide)
    (set vim.g.neovide_cursor_vfx_mode "pixiedust")))

(lambda nvim-qt []
  "Initialize the setup for nvim-qt."
  (when (= 2 (vim.fn.exists ":Guifont"))
    ;; Force the usage of the font
    (vim.cmd "Guifont! FantasqueSansMono\\ Nerd\\ Font:h16"))
  (when (= 2 (vim.fn.exists ":GuiTabline"))
    (vim.cmd "GuiTabline 1"))
  (when (= 2 (vim.fn.exists ":GuiPopupmenu"))
    (vim.cmd "GuiPopupmenu 1"))
  (when (vim.fn.exists "*GuiShowContextMenu")
    (let [utils (require :config.utils)]
      (utils.mapper "n" "<RightMouse>" ":call GuiShowContextMenu()<CR>")
      (utils.mapper "i" "<RightMouse>" "<Esc>:call GuiShowContextMenu()<CR>")
      (utils.mapper "x" "<RightMouse>" ":call GuiShowContextMenu()<CR>gv")
      (utils.mapper "s" "<RightMouse>" "<C-G>:call GuiShowContextMenu()<CR>gv"))))

(lambda init []
  "Initialize the setup for GUI front-ends."
  (set vim.o.guifont "FantasqueSansMono Nerd Font:h16")
  (nvim-qt)
  (fvim)
  (neovide))

{: init}
