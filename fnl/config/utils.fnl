;;; utils.fnl --- Utility functions to DRY

(lambda kbd [key-str]
  "Helper to escape termcodes like <Space> to normal values"
  (vim.api.nvim_replace_termcodes key-str true false true))

(lambda mapper [mode key result ?opts]
  "Remap globally KEY to RESULT in MODE.

 Leave MODE empty for :map."
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (vim.api.nvim_set_keymap mode key result opts)))

(lambda noremapper [mode key result ?opts]
  "Remap globally KEY to RESULT in MODE.

 Leave MODE empty for :noremap."
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (tset opts :noremap true)
    (vim.api.nvim_set_keymap mode key result opts)))

(lambda leader-mapper [mode key result ?opts]
  "Remap globally <Leader>KEY to RESULT in MODE.

 Leave MODE empty for :map"
  (mapper mode (.. "<Leader>" key) result ?opts))

(lambda localleader-mapper [mode key result bufnr ?opts]
  "Remap globally <Leader>KEY to RESULT in MODE.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :buffer bufnr)
    (mapper mode (.. "<LocalLeader>" key) result opts)))

(lambda buffer-mapper [mode key result bufnr ?opts]
  "Remap in current buffer KEY to RESULT in MODE.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (vim.api.nvim_buf_set_keymap bufnr mode key result opts)))

(lambda augroup [group-name]
  "Create an autocmd group"
  (vim.api.nvim_create_augroup (.. :gagbo_ group-name) {:clear true}))

(lambda autocmd [group-name cmds]
  "Create an autocmd group named GROUP-NAME, with all CMDS in the list"
  (vim.cmd (.. "augroup " group-name))
  (vim.cmd "autocmd!")
  (each [_ cmd (ipairs cmds)]
    (vim.cmd cmd))
  (vim.cmd "augroup END"))

(lambda fn-mapper [mode key result ?opts]
  "Remap globally KEY to a function RESULT in MODE.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (vim.keymap.set mode key result opts)))

(lambda fn-buffer-mapper [mode key result bufnr ?opts]
  "Remap in buffer BUFNR KEY to a function RESULT in MODE only in buffer BUFNR.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :buffer bufnr)
    (tset opts :silent true)
    (vim.keymap.set mode key result opts)))

(lambda fn-leader-mapper [mode key result ?opts]
  "Remap globally <Leader>KEY to RESULT in MODE.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (fn-mapper mode (.. "<Leader>" key) result opts)))

(lambda fn-localleader-mapper [mode key result bufnr ?opts]
  "Remap globally <Leader>KEY to RESULT in MODE.

 Leave MODE empty for :map"
  (let [opts (or ?opts {})]
    (tset opts :silent true)
    (tset opts :buffer bufnr)
    (fn-mapper mode (.. "<LocalLeader>" key) result opts)))

{: mapper
 : noremapper
 : leader-mapper
 : buffer-mapper
 : leader-mapper
 : localleader-mapper
 : augroup
 : autocmd
 : fn-mapper
 : fn-buffer-mapper
 : fn-leader-mapper
 : fn-localleader-mapper
 : kbd}
