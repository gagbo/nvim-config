(lambda open_nvim_tree [data]
  "Handler to decide on NvimTree on Open"
  (let [api (require :nvim-tree.api)
        ignored-fts [:startify :dashboard :alpha :gitcommit]
        real-file? (= 1 (vim.fn.filereadable data.file))
        no-name-buffer? (and (= "" data.file)
                             (= "" (. vim.bo data.buf :buftype)))
        ft (. vim.bo data.buf :ft)
        is-ignored-ft? (vim.tbl_contains ignored-fts ft)]

    (when (and
            (or real-file? no-name-buffer?)
            (not is-ignored-ft?))
      (api.tree.toggle {:focus false :find_file true}))))

(lambda init []
  (let
      [utils (require :config.utils)
       leader utils.leader-mapper]

    ;; Remove auto-open until better criteria are found.
    vim.api.nvim_create_autocmd [:VimEnter] {:callback open_nvim_tree}

    (leader "n" "op" "<cmd>NvimTreeToggle<CR>")
    ;; E : Explore
    (leader "n" "e" "<cmd>NvimTreeToggle<CR>")))

(lambda on_attach [bufnr]
  "On attach handler for nvim-tree"
  (let [api (require :nvim-tree.api)
        bufmapper (. (require :config.utils) :fn-buffer-mapper)]
    (api.config.mappings.default_on_attach bufnr)
    (bufmapper "n" "l" api.node.open.edit 0 {:desc "nvim-tree: Open"})
    (bufmapper "n" "h" api.node.navigate.parent_close 0 {:desc "nvim-tree: Close Directory"})
    (bufmapper "n" "v" api.node.open.vertical 0 {:desc "nvim-tree: Open: Vertical Split"})))

(lambda config []
  "Initialize nvim-tree."
  (let [tree (require :nvim-tree)]

    (tree.setup
     {: on_attach
      :disable_netrw true
      :hijack_netrw true
      :sync_root_with_cwd true
      :respect_buf_cwd true
      :diagnostics {:enable true
                    :icons {:hint "h"
                            :info "i"
                            :warning "w"
                            :error "e"}}
      :update_focused_file {:enable true
                            :update_cwd true
                            :ignore_list {}}
      :renderer {:icons {:glyphs {:default  ""
                                  :symlink  ""
                                  :git
                                  {:unstaged  ""
                                   :staged  "S"
                                   :unmerged  ""
                                   :renamed  "➜"
                                   :deleted  ""
                                   :untracked  "U"
                                   :ignored  "◌"}
                                  :folder
                                  {:default  ""
                                   :open  ""
                                   :empty  ""
                                   :empty_open  ""
                                   :symlink  ""}}}}
      :git {:enable true
            :ignore true
            :timeout 500}
      :view {:width  30
             :hide_root_folder  false
             :side  "left"
             :number false
             :relativenumber  false}})))

[{1 "nvim-tree/nvim-tree.lua"
  :dependencies ["nvim-tree/nvim-web-devicons"]
  : config
  :cmd ["NvimTreeToggle"]
  : init
  :lazy false}]
