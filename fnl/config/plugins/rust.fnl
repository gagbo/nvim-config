(fn crates-config []
 (let [crates (require :crates)]
   (crates.setup)))

[{1 "Saecki/crates.nvim"
  :dependencies ["nvim-lua/plenary.nvim"]
  :config crates-config}
 "rust-lang/rust.vim"]
