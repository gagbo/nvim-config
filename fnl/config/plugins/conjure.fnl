(lambda init []
  (set vim.g.conjure#filetype#fennel  "conjure.client.fennel.aniseed")
  (set vim.g.conjure#client#fennel#aniseed#aniseed_module_prefix  "aniseed.")
  (set vim.g.conjure#filetypes [:clojure :fennel :janet :hy :julia :racket :scheme :lisp :python]))

{1 "Olical/conjure" :dependencies ["Olical/aniseed"] : init}
