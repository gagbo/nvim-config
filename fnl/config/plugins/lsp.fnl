(local utils (require :config.utils))

(lambda config-lsp []
  (local has-lsp?  (pcall require :lspconfig))

  (when has-lsp?
    (local lsp (require :lspconfig))
    (local cmp (require :cmp_nvim_lsp))
    (local rust_tools (require :rust-tools))
    (rust_tools.setup)
    (lambda on_attach [_ bufnr]
      (let [telescope (require :telescope.builtin)]
        ;; LSP specific mappings
        (utils.fn-buffer-mapper "n" "<leader>cf" (fn [] (vim.lsp.buf.format {:async true})) bufnr { :desc "LSP: [C]ode [F]ormat"})
        (utils.fn-buffer-mapper "n" "<leader>ca" vim.lsp.buf.code_action bufnr { :desc "LSP: [C]ode [A]ction"})
        (utils.fn-buffer-mapper "n" "<leader>cr" vim.lsp.buf.rename bufnr { :desc "LSP: [R]ename"})
        (utils.fn-buffer-mapper "n" "gD" vim.lsp.buf.declaration bufnr { :desc "LSP: [g]o to [D]eclaration"})
        (utils.fn-buffer-mapper "n" "gi" vim.lsp.buf.implementation bufnr { :desc "LSP: [g]o to [I]mplementation"})
        (utils.fn-buffer-mapper "n" "gd" vim.lsp.buf.definition bufnr { :desc "LSP: Go do definition"})
        (utils.fn-buffer-mapper "n" "K" vim.lsp.buf.hover bufnr { :desc "LSP: hover"})
        (utils.fn-buffer-mapper "n" "<c-k>" vim.lsp.buf.signature_help bufnr { :desc "LSP: Signature help"})
        (utils.fn-buffer-mapper "n" "<leader>D" vim.lsp.buf.type_definition bufnr { :desc "LSP: Type [D]efinition"})
        (utils.buffer-mapper "n" "gr" "<cmd>lua require('telescope.builtin').lsp_references{ shorten_path = true }<CR>" bufnr)
        (utils.buffer-mapper "n" "gR" "<cmd>TroubleToggle lsp_references<CR>" bufnr)
        (utils.fn-buffer-mapper "n" "<leader>ds" telescope.lsp_document_symbols bufnr { :desc "LSP: [D]ocument [S]ymbols"})
        (utils.fn-buffer-mapper "n" "<leader>cj" telescope.lsp_dynamic_workspace_symbols bufnr { :desc "LSP: [C]ode [J]ump"})

        ;; Set lsp omnifunc
        (vim.api.nvim_buf_set_option bufnr "omnifunc" "v:lua.vim.lsp.omnifunc")))

    (local capabilities (cmp.default_capabilities (vim.lsp.protocol.make_client_capabilities)))

    ;; Default servers configuration
    (let [servers {:clangd "clangd"
                   :cmake "cmake-language-server"
                   :rust_analyzer "rust-analyzer"
                   :gopls "gopls"
                   :golangci_lint_ls "golangci-lint-langserver"
                   :ts_ls "typescript-language-server"
                   :svelte "svelteserver"
                   :tailwindcss "tailwindcss-language-server"
                   :beancount "beancount-language-server"
                   :zls "zls"
                   :pyright "pyright"
                   :terraformls "terraform-ls"}]
      (each [server _binary (pairs servers)]
          ((. (. lsp server) :setup)
           {: on_attach
            : capabilities})))

    ;; Lua specific LSP config
    (when (= 1 (vim.fn.executable "lua-language-server"))
      (let [runtime-path (vim.split package.path ";")]
        (table.insert runtime-path "lua/?.lua")
        (table.insert runtime-path "lua/?/init.lua")
        (lsp.lua_ls.setup
         {: on_attach
          : capabilities
          :settings {:Lua {:runtime {:version :LuaJIT
                                     :path runtime-path}
                           :diagnostics {:globals [:vim]}
                           :workspace {:library (vim.api.nvim_get_runtime_file "" true)
                                       :checkThirdParty false}}}})))

    ;; Fennel specific config
    (lsp.fennel_language_server.setup
      {: on_attach
       : capabilities
       :settings {:fennel {:workspace {:library (vim.api.nvim_list_runtime_paths)}
                           :diagnostics {:globals [:vim]}}}})))

(lambda dapconfig []
  "Configure DAP adapters."
  (local has-dap?  (pcall require :dap))
  (when has-dap?
    (let [dap (require :dap)]
      (set dap.adapters.lldb {:type "executable" :command (vim.fn.exepath "lldb-vscode") :name "lldb"})
      (let [conf {1 {:name "Launch"
                     :type "lldb"
                     :request "launch"
                     :program (lambda [] vim.fn.input "Path to executable: " (.. (vim.fn.getcwd) "/") "file")
                     :cwd "${workspaceFolder}"
                     :stopOnEntry false
                     :args {}}}]
        (set dap.configurations.cpp conf)
        (set dap.configurations.c conf)
        (set dap.configurations.rust conf)))))

(lambda config-dap []
  "Initialize the DAP and DAP-UI packages"
  (local has-dap?  (pcall require :dap))
  (local has-dap-ui?  (pcall require :dapui))
  (when (and has-dap? has-dap-ui?)
    (let [dap (require :dap)
          dapui (require :dapui)]

      (dapconfig)

      ;; DAP specific mappings
      (utils.fn-mapper "n" "<F5>" dap.continue { :desc "DAP: Continue"})
      (utils.fn-mapper "n" "<F10>" dap.step_over { :desc "DAP: Step Over"})
      (utils.fn-mapper "n" "<F11>" dap.step_into { :desc "DAP: Step Into"})
      (utils.fn-mapper "n" "<F12>" dap.step_out { :desc "DAP: Step Out"})
      (utils.fn-mapper "n" "<leader>db" dap.toggle_breakpoint { :desc "DAP: Breakpoint"})
      (utils.mapper "n" "<leader>dB" "<Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>" { :desc "DAP: Conditional Breakpoint"})
      (utils.mapper "n" "<leader>dlp" "<Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>" { :desc "DAP: Log point"})
      (utils.fn-mapper "n" "<leader>drr" dap.repl.open { :desc "DAP: [R]EPL Open"})
      (utils.fn-mapper "n" "<leader>drl" dap.run_last { :desc "DAP: Run [L]ast"})

      ;; DAP UI automated windows
      (tset dap.listeners.after.event_initialized "dapui_config" dapui.open)
      (tset dap.listeners.after.event_terminated "dapui_config" dapui.close)
      (tset dap.listeners.after.event_exited "dapui_config" dapui.close)

      (dapui.setup))))

[{1 "neovim/nvim-lspconfig"
  :dependencies ["simrat39/rust-tools.nvim"
                 "williamboman/mason-lspconfig.nvim"]
  :config config-lsp}
 {1 "mfussenegger/nvim-dap"
  :dependencies ["nvim-lua/plenary.nvim"
                 "williamboman/mason.nvim"]
  :config config-dap}
 {1 "rcarriga/nvim-dap-ui"  :dependencies ["mfussenegger/nvim-dap"]}]
