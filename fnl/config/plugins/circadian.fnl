(fn config []
  (let [circadian (require :circadian)
        opts {:lat 48.8567879
              :lon 2.3510768
              :day {:background :light
                    :colorscheme :zenbones}
              :night {:background :dark
                      :colorscheme :kanagawabones}}]
    (circadian.setup opts)))

[{1 "gagbo/circadian.nvim" : config :lazy false :priority 900}]
