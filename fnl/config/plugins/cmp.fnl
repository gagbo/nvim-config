(fn config []
  (let [cmp (require :cmp)
        luasnip (require :luasnip)
        has-words-before
        (lambda []
          (let [(line col) (unpack (vim.api.nvim_win_get_cursor 0))]
            (and
             (not= col 0)
             (-> (vim.api.nvim_buf_get_lines 0 (- line 1) line true)
                 (. 1)
                 (: :sub col col)
                 (: :match "%s")
                 (= nil)))))]
    (cmp.setup {:enabled true
                :window_documentation "native"
                :snippet {:expand (lambda [args] (luasnip.lsp_expand (. args "body")))}
                :mapping {:<C-d> (cmp.mapping.scroll_docs (- 4))
                          :<C-f> (cmp.mapping.scroll_docs 4)
                          :<C-n> (cmp.mapping.select_next_item {:behaviour cmp.SelectBehavior.Insert})
                          :<C-p> (cmp.mapping.select_prev_item {:behaviour cmp.SelectBehavior.Insert})
                          :<Down> (cmp.mapping.select_next_item {:behaviour cmp.SelectBehavior.Select})
                          :<Up> (cmp.mapping.select_prev_item {:behaviour cmp.SelectBehavior.Select})
                          :<Tab> (cmp.mapping
                                  (lambda [fallback]
                                    (if (cmp.visible)
                                        (cmp.select_next_item)
                                        ;; Second cond
                                        (luasnip.expand_or_jumpable)
                                        (luasnip.expand_or_jump)
                                        ;; Third cond
                                        (has-words-before)
                                        (cmp.complete)
                                        ;; Fallback
                                        (fallback)))
                                  ["i" "s"])
                          :<S-Tab> (cmp.mapping
                                    (lambda [fallback]
                                      (if (cmp.visible)
                                          (cmp.select_prev_item)
                                          ;; Second cond
                                          (luasnip.jumpable (- 1))
                                          (luasnip.jump (- 1))
                                          ;; Fallback
                                          (fallback)))
                                    ["i" "s"])
                          :<C-Space> (cmp.mapping.complete)
                          :<C-e> (cmp.mapping.abort)
                          :<CR> (cmp.mapping.confirm {:select false})}
                :sources [{:name "orgmode"}
                          {:name "luasnip"}
                          {:name "nvim_lsp"}
                          {:name "conjure"}
                          {:name "crates"}
                          {:name "buffer"}]})))


[{1 "hrsh7th/nvim-cmp"
  :dependencies ["hrsh7th/cmp-nvim-lsp"
                 "hrsh7th/cmp-buffer"
                 "saadparwaiz1/cmp_luasnip"
                 "PaterJason/cmp-conjure"
                 "L3MON4D3/LuaSnip"
                 "nvim-orgmode/orgmode"
                 "Saecki/crates.nvim"]
  : config
  :event :InsertEnter}]
