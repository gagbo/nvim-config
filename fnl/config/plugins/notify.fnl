(fn dismiss-all []
  "Dismiss all notifications."
  ((. (require :notify) :dismiss) {:silent true}))

(fn config []
 (let [notify (require :notify)
       utils (require :config.utils)
       fn-mapper utils.fn-mapper]
   (notify.setup {:timeout 2000 :fps 60})
   (set vim.notify notify)
   ;; Clean-up all notifications when entering insert mode
   (vim.api.nvim_create_autocmd [:InsertEnter] {:group (vim.api.nvim_create_augroup :NotifyClearGrp {})
                                                :pattern "*"
                                                :callback dismiss-all})
   (fn-mapper "n" "<Esc>" dismiss-all)))

[{1 "rcarriga/nvim-notify"
  : config
  :lazy false
  :priority 1000}]
