(fn init []
  (let [utils (require :config.utils)
             mapper utils.mapper
             fn-mapper utils.fn-mapper
             leader utils.leader-mapper
             fn-leader utils.fn-leader-mapper]
   ;; X : Diagostics
   (leader "n" "xx" "<cmd>TroubleToggle<CR>")
   (leader "n" "xd" "<cmd>TroubleToggle document_diagnostics<CR>")
   (leader "n" "xw" "<cmd>TroubleToggle workspace_diagnostics<CR>")
   (leader "n" "xn" "<cmd>TroubleToggle<CR>")
   (leader "n" "xp" "<cmd>TroubleToggle<CR>")
   (leader "n" "xl" "<cmd>TroubleToggle<CR>")))

(fn config []
 (let [trouble (require :trouble)]
   (trouble.setup {:signs {:error ""
                           :warning ""
                           :hint ""
                           :information ""
                           :other "﫠"}})))

[{1 "folke/trouble.nvim"
  :dependencies ["nvim-tree/nvim-web-devicons"]
  :cmd ["TroubleToggle"]
  : init
  : config}]
