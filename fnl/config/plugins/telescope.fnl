(lambda init []
  (let [utils (require :config.utils)
             mapper utils.mapper
             fn-mapper utils.fn-mapper
             leader utils.leader-mapper
             fn-leader utils.fn-leader-mapper]

   (leader "n" "<Space>" "<cmd>lua require('telescope.builtin').git_files{ show_untracked = true }<CR>")
   (leader "n" "." "<cmd>Telescope file_browser path=%:p:h<CR>")

   (leader "n" "'" "<cmd>lua require('telescope.builtin').resume{}<CR>")

   (leader "n" "bb" "<cmd>lua require('telescope.builtin').buffers{ show_all_buffers = true }<CR>")
   ;; H : Help
   (leader "n" "hk" "<cmd>lua require('telescope.builtin').keymaps{}<CR>")
   ;; F : File
   (leader "n" "fr" "<cmd>lua require('telescope.builtin').oldfiles{}<CR>")
   (leader "n" "fg" "<cmd>lua require('telescope.builtin').git_files{}<CR>")
   (leader "n" "fp" "<cmd>lua require('telescope.builtin').find_files{ cwd = '~/.config/nvim'}<CR>")
   ;; S : Search
   (leader "n" "sp" "<cmd>lua require('telescope.builtin').live_grep{}<CR>")
   (leader "n" "sr" "<cmd>lua require('telescope.builtin').lsp_references{}<CR>")
   (leader "n" "ss" "<cmd>lua require('telescope.builtin').lsp_workspace_symbols{}<CR>")
   ;; L : Lists
   (leader "n" "ll" "<cmd>lua require('telescope.builtin').loclist{}<CR>")
   (leader "n" "lq" "<cmd>lua require('telescope.builtin').quickfix{}<CR>")))

(lambda config []
 (let [telescope (require :telescope)
       telescope-themes (require :telescope.themes)]
   (telescope.setup
     {:extensions
       {:ui-select [(telescope-themes.get_dropdown {})]
        :file_browser {:hijack_netrw true
                       :theme :ivy}}})
   (telescope.load_extension :ui-select)
   (telescope.load_extension :projects)
   (telescope.load_extension :file_browser)))

[{1 "nvim-telescope/telescope.nvim"
  :dependencies ["nvim-lua/plenary.nvim"
                 "nvim-telescope/telescope-ui-select.nvim"
                 "nvim-telescope/telescope-file-browser.nvim"
                 "ahmedkhalf/project.nvim"]
  : config
  : init
  :cmd ["Telescope"]}]
