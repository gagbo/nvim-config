(fn init []
  (let [utils (require :config.utils)
        mapper utils.mapper
        fn-mapper utils.fn-mapper
        leader utils.leader-mapper
        fn-leader utils.fn-leader-mapper]

;;; gs + w/b e/B to navigate to word beginnings and endings
    (mapper "" "gsw" "<cmd>lua require('hop').hint_words({direction = require('hop.hint').HintDirection.AFTER_CURSOR})<CR>")
    (mapper "" "gse" "<cmd>lua require('hop').hint_words({direction = require('hop.hint').HintDirection.AFTER_CURSOR, hint_position = require('hop.hint').HintPosition.END})<CR>")
    (mapper "" "gsB" "<cmd>lua require('hop').hint_words({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, hint_position = require('hop.hint').HintPosition.END})<CR>")
    (mapper "" "gsb" "<cmd>lua require('hop').hint_words({direction = require('hop.hint').HintDirection.BEFORE_CURSOR})<CR>")
;;; gs + t/s to navigate to lines
    (mapper "n" "gst" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.AFTER_CURSOR})<CR>")
    (mapper "o" "gst" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.AFTER_CURSOR, inclusive_jump = true})<CR>")
    (mapper "n" "gss" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.BEFORE_CURSOR})<CR>")
    (mapper "o" "gss" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, inclusive_jump = true})<CR>")
;;; f/j to navigate to characters
    ;; (mapper "n" "f" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.AFTER_CURSOR, current_line_only = true})<CR>")
    ;; (mapper "n" "F" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, current_line_only = true})<CR>")
    ;; (mapper "o" "f" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.AFTER_CURSOR, current_line_only = true, inclusive_jump = true})<CR>")
    ;; (mapper "o" "F" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, current_line_only = true, inclusive_jump = true})<CR>")
    ;; (mapper "" "t" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.AFTER_CURSOR, current_line_only = true})<CR>")
    ;; (mapper "" "T" "<cmd>lua require('hop').hint_char1({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, current_line_only = true})<CR>")

    ;; Hop motions
    (leader "n" "n" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.AFTER_CURSOR})<CR>" { :noremap true})
    (leader "v" "n" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.AFTER_CURSOR})<CR>" { :noremap true})
    (leader "o" "n" "<cmd>lua require('hop').hint_lines({direction = require('hop.hint').HintDirection.AFTER_CURSOR, inclusive_jump = true})<CR>" { :noremap true})
    (leader "n" "e" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.BEFORE_CURSOR})<CR>" { :noremap true})
    (leader "v" "e" "<cmd>lua require('hop').hint_lines_skip_whitespace({direction = require('hop.hint').HintDirection.BEFORE_CURSOR})<CR>" { :noremap true})
    (leader "o" "e" "<cmd>lua require('hop').hint_lines({direction = require('hop.hint').HintDirection.BEFORE_CURSOR, inclusive_jump = true})<CR>" { :noremap true})
    (leader "n" "f" "<cmd>lua require('hop').hint_words({ hint_position = require('hop.hint').HintPosition.END})<CR>" { :noremap true})
    (leader "v" "f" "<cmd>lua require('hop').hint_words({ hint_position = require('hop.hint').HintPosition.END})<CR>" { :noremap true})
    (leader "o" "f" "<cmd>lua require('hop').hint_words({ hint_position = require('hop.hint').HintPosition.END, inclusive_jump = true})<CR>" { :noremap true})))

(fn config []
 (let [hop (require :hop)]
   (hop.setup {:keys "tnsedhc,flpo" :jump_on_sole_occurrence true})))

{1 "phaazon/hop.nvim"
 : config
 : init}
