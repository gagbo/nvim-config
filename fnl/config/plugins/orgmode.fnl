(lambda config []
  (let [orgmode (require :orgmode)
        ts (require :nvim-treesitter.configs)]
    (orgmode.setup_ts_grammar)
    (ts.setup
     {:highlight {:enable true :additional_vim_regex_highlighting [:org]}
      :ensure_installed [:org]})
    (orgmode.setup
     {:org_agenda_files ["/hdd-data/nextCloud/org-roam/"]
      :org_default_notes_files "/hdd-data/nextCloud/org/inbox.org"
      :mappings {:global {:org_agenda ["gA" "<Leader>oa"]
                          :org_capture ["gC" "<Leader>oc"]}}})))

[{1 "nvim-orgmode/orgmode"
  :dependencies ["nvim-treesitter/nvim-treesitter"]
  : config
  :ft [:org]
  :keys ["gA" "gC" "<Leader>oa" "<Leader>oc"]}]
