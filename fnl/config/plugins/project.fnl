(fn config []
  (let [project (require :project_nvim)]
    (project.setup {:silent_chdir false})))

[{1  "ahmedkhalf/project.nvim" : config}]
