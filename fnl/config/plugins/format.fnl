(fn init []
  (let [utils (require :config.utils)
        leader utils.leader-mapper
        augroup utils.augroup]
    (leader "n" "f" "<cmd>Format<CR>" { :noremap true})
    (leader "n" "F" "<cmd>FormatWrite<CR>" { :noremap true})
    ;;; Format on save
    (vim.api.nvim_create_autocmd
         [:BufWritePost]
         {:group (augroup "FormatAuto")
              :command "FormatWrite"})))

(fn config []
  (let [formatter (require :formatter)]
    (formatter.setup {:logging true
                      :log_level vim.log.levels.WARN
                      :filetype {"*" [(. (require :formatter.filetypes.any) :remove_trailing_whitespace)]
                                 :c (let [builtins (require :formatter.filetypes.c)]
                                      [builtins.clangformat])
                                 :cpp (let [builtins (require :formatter.filetypes.cpp)]
                                        [builtins.clangformat])
                                 :lua (let [builtins (require :formatter.filetypes.lua)]
                                        [builtins.stylua])
                                 :go (let [builtins (require :formatter.filetypes.go)]
                                       [builtins.goimports builtins.gofmt])
                                 :rust (let [builtins (require :formatter.filetypes.rust)]
                                         [builtins.rustfmt])
                                 :zig (let [builtins (require :formatter.filetypes.zig)]
                                        [builtins.zigfmt])
                                 :python (let [builtins (require :formatter.filetypes.python)]
                                           [builtins.isort builtins.black])
                                 :typescript (let [builtins (require :formatter.filetypes.typescript)]
                                               [builtins.prettiereslint builtins.prettier])}})))




[{1 "mhartington/formatter.nvim"
  : config
  : init
  :cmd [:Format :FormatWrite]}]
