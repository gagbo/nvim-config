(fn config []
 (let [dressing (require :dressing)]
   (dressing.setup)))

[{1
  "stevearc/dressing.nvim" : config
  :event :VeryLazy}]
