(lambda config-mason []
  (let [mason (require :mason)]
    (mason.setup {:ui {:icons
                       {:package_installed "✓"
                        :package_pending "➜"
                        :package_uninstalled "✗"}}})))

(lambda config-mason-lsp []
  (let [mason-lsp (require :mason-lspconfig)]
    (mason-lsp.setup {:ensure_installed [:lua_ls :fennel_language_server]})))

[{1 "williamboman/mason.nvim"
  :config config-mason}
 {1 "williamboman/mason-lspconfig.nvim"
  :dependencies ["williamboman/mason.nvim"]
  :config config-mason-lsp}]
