(local has-builtin-playground (> (vim.fn.exists ":EditQuery") 0))

(lambda config []
      (let [conf (require :nvim-treesitter.configs)]
        (conf.setup
         {:ensure_installed [:lua :rust :vim]
          :highlight {:enable true}
          :indent {:enable true}
          :incremental_selection {:enable true
                                  :keymaps {:init_selection "gnn"
                                            :node_incremental "grn"
                                            :scope_incremental "grc"
                                            :node_decremental "grm"}}
          :textobjects {:select {:enable true
                                 :lookahead true
                                 :keymaps {:af "@function.outer"
                                           :if "@function.inner"
                                           :ac "@class.outer"
                                           :ic "@class.inner"}}
                        :move {:enable true
                               :set_jumps true
                               :goto_next_start {"]m" "@function.outer" "]]" "@class.outer"}
                               :goto_next_end {"]M" "@function.outer" "][" "@class.outer"}
                               :goto_previous_start {"[m" "@function.outer" "[[" "@class.outer"}
                               :goto_previous_end {"[M" "@function.outer" "[]" "@class.outer"}}}
          :playground {:enable (not has-builtin-playground)
                       :disable {}
                       :updatetime 25
                       :persist_queries true
                       :keybindings {:toggle_query_editor "o"
                                     :toggle_hl_groups "i"
                                     :toggle_injected_languages "t"
                                     :toggle_anonymous_nodes "a"
                                     :toggle_language_display "I"
                                     :focus_language "f"
                                     :unfocus_language "F"
                                     :update "R"
                                     :goto_node "<cr>"
                                     :show_help "?"}}})))
(if has-builtin-playground
   [{1 "nvim-treesitter/nvim-treesitter" : config}
    "nvim-treesitter/nvim-treesitter-textobjects"]

 [{1 "nvim-treesitter/nvim-treesitter" : config}
  "nvim-treesitter/nvim-treesitter-textobjects"
  "nvim-treesitter/playground"])

