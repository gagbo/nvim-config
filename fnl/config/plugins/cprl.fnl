(fn config []
;; TODO: Make a function to detect properly the mode
;; use `git branch --show-current` or the SHA of the commit to know the reference we want to use
;; and then i.e. detect "github/srg/srht" from
;; `git remote get-url "$(git config --get "branch.$(git branch --show-current).remote")"`
;; and then use `git branch --show-current` or the SHA of the commit to know the reference we want to use
 (let [cprl (require :cprl)]
   (cprl.setup {:host
                {:srht (fn [host repo ref path firstline lastline]
                                ;; https://git.sr.ht/REPO/tree/REF/item/PATH?view-source#LN-M
                           (let [line (if (= firstline lastline)
                                        (string.format "#L%d" firstline)
                                        (string.format "#L%d-%d" firstline lastline))]
                             (string.format "https://%s/%s/tree/%s/item/%s?view-source%s" host repo ref path line)))}})))


{1 "knsh14/cprl.nvim"
 : config
 :cmd ["CopyRemoteLink"]
 :dependencies ["rcarriga/nvim-notify"]}
