(lambda init []
  (let [utils (require :config.utils)
        mapper utils.mapper
        fn-mapper utils.fn-mapper
        leader utils.leader-mapper
        fn-leader utils.fn-leader-mapper]
    ;; G : Git
    (leader "n" "gg" "<cmd>Neogit<CR>")))

(fn config []
  (let [neogit (require :neogit)]
    (neogit.setup {:use_magit_keybindings true :disable_insert_on_commit false})))

[{1 "TimUntersberger/neogit"
  :dependencies ["nvim-lua/plenary.nvim"]
  : config 
  :cmd ["Neogit"]
  : init}]
